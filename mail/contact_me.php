<?php
// Check for empty fields
if(empty($_POST['name'])  || empty($_POST['phone']) || empty($_POST['message']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
  http_response_code(500);
  exit();
}

$name = strip_tags(htmlspecialchars($_POST['name']));
$email = strip_tags(htmlspecialchars($_POST['email']));
$phone = strip_tags(htmlspecialchars($_POST['phone']));
$message = strip_tags(htmlspecialchars($_POST['message']));

// Create the email and send the message
$to = "thegevssss@gmail.com"; // Add your email address inbetween the "" replacing yourname@yourdomain.com - This is where the form will send a message to.
$subject = "Een bericht van $name vanuit TechChamp.be";
$body = "Een nieuwe mail van u website\n\n"."De details:\n\nNaam: $name\n\nEmail: $email\n\nPhone: $phone\n\nBericht:\n$message";
$header = "From: noreply@techchamp.com\n"; // This is the email address the generated message will be from. We recommend using something like noreply@yourdomain.com.
$header .= "Reply-To: $email";	
mail($to, $subject, $body, $header);
if(!mail($to, $subject, $body, $header))
  http_response_code(500);
?>
